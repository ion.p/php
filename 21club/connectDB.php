<?php
include('configure.php');
$servername = $server_name;
$username = $user_name;
$password = $passw;
$dbname = $db_name;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>