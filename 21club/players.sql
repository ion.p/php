CREATE TABLE players (
    `player_id` INT,
    `team_id` INT,
    `team_name` VARCHAR(9) CHARACTER SET utf8,
    `jersey_num` INT,
    `position` VARCHAR(10) CHARACTER SET utf8,
    `full_position` VARCHAR(20) CHARACTER SET utf8,
    `side` VARCHAR(17) CHARACTER SET utf8,
    `first_name` VARCHAR(15) CHARACTER SET utf8,
    `last_name` VARCHAR(20) CHARACTER SET utf8,
    `birth_date` VARCHAR(16) CHARACTER SET utf8,
    `birth_place` VARCHAR(17) CHARACTER SET utf8,
    `country` VARCHAR(13) CHARACTER SET utf8,
    `first_nationality` VARCHAR(22) CHARACTER SET utf8,
    `height` INT,
    `known_name` VARCHAR(15) CHARACTER SET utf8,
    `preferred_foot` VARCHAR(5) CHARACTER SET utf8,
    `weight` INT
);
INSERT INTO players VALUES (9631,14,'Liverpool',1,'Goalkeeper','Goalkeeper','Unknown','Brad','Jones','19/03/1982 00:00','Perth','Australia','Australia',191,NULL,NULL,76);
INSERT INTO players VALUES (57328,14,'Liverpool',2,'Defender','Full Back','Right','Nathaniel','Clyne','04/04/1991 23:00',NULL,'England',NULL,175,NULL,NULL,67);
INSERT INTO players VALUES (26725,14,'Liverpool',3,'Defender','Full Back','Left','Jose','Enrique Sanchez Diaz','23/01/1986 00:00',NULL,'Spain',NULL,184,NULL,NULL,76);
INSERT INTO players VALUES (12450,14,'Liverpool',4,'Defender','Central Defender','Centre','Kolo','Toure','19/03/1981 00:00','Bouaké','Côte d''Ivoire','Côte d''Ivoire',183,NULL,NULL,74);
INSERT INTO players VALUES (21094,14,'Liverpool',5,'Defender','Central Defender','Centre','Daniel','Agger','12/12/1984 00:00',NULL,'Denmark',NULL,188,NULL,NULL,79);
INSERT INTO players VALUES (38454,14,'Liverpool',6,'Defender','Central Defender','Centre','Dejan','Lovren','04/07/1989 23:00','Zenica','Croatia','Bosnia and Herzegovina',188,NULL,'Both',84);
INSERT INTO players VALUES (15157,14,'Liverpool',7,'Midfielder','Attacking Midfielder','Left/Centre/Right','James','Milner','04/01/1986 00:00','Leeds','England','England',175,NULL,NULL,70);
INSERT INTO players VALUES (1814,14,'Liverpool',8,'Midfielder','Defensive Midfielder','Centre','Steven','Gerrard','29/05/1980 23:00','Whiston','England','England',185,NULL,NULL,82);
INSERT INTO players VALUES (54861,14,'Liverpool',9,'Forward','Striker','Centre','Christian','Benteke','03/12/1990 00:00','Kinshasa','Belgium','Congo DR',190,NULL,'Right',83);
INSERT INTO players VALUES (84583,14,'Liverpool',10,'Midfielder','Attacking Midfielder','Centre','Philippe','Coutinho','11/06/1992 23:00','Rio de Janeiro','Brazil','Brazil',171,NULL,'Right',71);
INSERT INTO players VALUES (92217,14,'Liverpool',11,'Forward','Second Striker','Left/Centre/Right','Roberto Firmino','Barbosa de Oliveira','01/10/1991 23:00','Maceio','Brazil','Brazil',181,'Roberto Firmino','Right',76);
INSERT INTO players VALUES (171287,14,'Liverpool',12,'Defender','Central Defender','Centre','Joseph','Gomez','22/05/1997 23:00',NULL,'England',NULL,188,NULL,NULL,77);
INSERT INTO players VALUES (56979,14,'Liverpool',14,'Midfielder','Central Midfielder','Centre/Right','Jordan','Henderson','16/06/1990 23:00','Sunderland','England','England',182,NULL,NULL,67);
INSERT INTO players VALUES (40755,14,'Liverpool',15,'Forward','Striker','Left/Centre/Right','Daniel','Sturridge','31/08/1989 23:00','Birmingham','England','England',188,NULL,NULL,76);
INSERT INTO players VALUES (40784,14,'Liverpool',17,'Defender','Central Defender','Left/Centre','Mamadou','Sakho','13/02/1990 00:00','Paris','France','France',187,NULL,NULL,83);
INSERT INTO players VALUES (100059,14,'Liverpool',18,'Defender','Full Back','Left','Alberto','Moreno','04/07/1992 23:00','Sevilla','Spain','Spain',170,NULL,'Left',65);
INSERT INTO players VALUES (68815,14,'Liverpool',19,'Defender','Central Defender','Centre','Steven','Caulker','29/12/1991 00:00',NULL,'England',NULL,191,NULL,NULL,76);
INSERT INTO players VALUES (39155,14,'Liverpool',20,'Midfielder','Attacking Midfielder','Left/Centre','Adam','Lallana','09/05/1988 23:00','St Albans','England','England',173,NULL,NULL,73);
INSERT INTO players VALUES (43191,14,'Liverpool',21,'Midfielder','Central Midfielder','Centre','Lucas','Leiva','09/01/1987 00:00',NULL,'Brazil',NULL,179,'Lucas Leiva',NULL,74);
INSERT INTO players VALUES (66797,14,'Liverpool',22,'Goalkeeper','Goalkeeper','Unknown','Simon','Mignolet','06/03/1988 00:00','Sint-Truiden','Belgium','Belgium',193,NULL,NULL,87);
INSERT INTO players VALUES (112338,14,'Liverpool',23,'Midfielder','Central Midfielder','Centre/Right','Emre','Can','12/01/1994 00:00','Frankfurt am Main','Germany','Germany',184,NULL,'Right',82);
INSERT INTO players VALUES (40555,14,'Liverpool',24,'Midfielder','Central Midfielder','Centre','Joe','Allen','14/03/1990 00:00',NULL,'Wales',NULL,168,NULL,NULL,62);
INSERT INTO players VALUES (102548,14,'Liverpool',26,'Defender','Central Defender','Centre','Tiago','Ilori','26/02/1993 00:00','London','Portugal','England',190,NULL,NULL,80);
INSERT INTO players VALUES (152760,14,'Liverpool',27,'Forward','Striker','Centre','Divock','Origi','17/04/1995 23:00','Ostende','Belgium','Belgium',185,NULL,NULL,75);
INSERT INTO players VALUES (84939,14,'Liverpool',28,'Forward','Striker','Centre','Danny','Ings','22/07/1992 23:00',NULL,'England',NULL,178,NULL,NULL,73);
INSERT INTO players VALUES (147668,14,'Liverpool',32,'Midfielder','Central Midfielder','Centre','Cameron','Brannagan','08/05/1996 23:00',NULL,'England',NULL,180,NULL,NULL,71);
INSERT INTO players VALUES (40400,14,'Liverpool',32,'Defender','Full Back','Right','Stephen','Darby','05/10/1988 23:00',NULL,'England',NULL,183,NULL,NULL,75);
INSERT INTO players VALUES (103912,14,'Liverpool',33,'Midfielder','Winger','Left/Right','Jordon','Ibe','08/12/1995 00:00',NULL,'England',NULL,176,NULL,NULL,81);
