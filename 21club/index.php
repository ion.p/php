<!DOCTYPE html>
<html lang="en">
<head>
  <title>21Club</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
<nav class="navbar navbar-inverse navbar-fixed">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">21Club</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
    </ul>
  </div>
</nav>
<div class="table-responsive">
    <div class="row">
		
		<div class="col-md-1">Sort by:</div>
		<div class="col-md-1">
		  <div class="dropdown">
			<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Select
			<span class="caret"></span></button>
			<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=player_id">ID</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=last_name">Name</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=team">Team name</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=position">Position</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=side">Side</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=country">Country</a></li>
			  <li role="presentation"><a role="menuitem" tabindex="-1" href="?var=known_name">Known name</a></li>
			</ul>
		  </div>			
		</div>

    </div>
	<table class="table table-bordered table-hover">
		<thead>
		  <tr>
			<th>Player ID</th>
			<th>Team name</th>
			<th>Jersey number</th>
			<th>Position</th>
			<th>Full Position</th>
			<th>Side</th>
			<th>First name</th>
			<th>Last name</th>
			<th>Birth date</th>
			<th>Birth place</th>
			<th>Country</th>
			<th>First nationality</th>
			<th>Height</th>
			<th>Known name</th>
			<th>Prefered foot</th>
			<th>Weight</th>
		  </tr>
		</thead>
		<tbody>

			<?php
			  include_once('connectDB.php');
			  if (isset($_GET['var'])){
				 $sql = "select * from players ORDER BY ".$_GET['var']." DESC";
			  }else{
				 $sql = "select * from players";				  
			  }
			  $result = $conn->query($sql);
			  if ($result->num_rows > 0) {
				 while($row = $result->fetch_assoc()) {
					echo'<tr>'; 
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["player_id"].'</a></td>';					
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["team_name"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["jersey_num"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["position"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["full_position"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["side"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["first_name"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["last_name"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["birth_date"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["birth_place"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["country"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["first_nationality"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["height"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["known_name"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["preferred_foot"].'</a></td>';
					echo '<td><a href="?id='.$row["player_id"].'">'.$row["weight"].'</a></td>';
					echo'</tr>';
				 }
			  }else {
				echo "0 results";
			  }
			?>

		</tbody>
	</table>
	

</div>
</div>
  
</body>
</html>
